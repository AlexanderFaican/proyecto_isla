from django.contrib import admin

# Register your models here.
from .models import Nota, Asistencia, Persona, Carrera, Docente, Ciclo, Evento, Asignatura, Estudiante
class adminAsignatura(admin.ModelAdmin):
    list_display = ["codigo_asignatura","nombre_asignatura"]
    class Meta:
             model=Asignatura
class adminAsistencia(admin.ModelAdmin):
    list_display = ["nombre_asignatura","nombre_estudiante","nombre_docente","numero_horas","numero_creditos","total_asistencias","total_inasistencia"]
    class Meta:
             model=Asistencia
class adminCarrera(admin.ModelAdmin):
    list_display = ["codigo_carrera","nombre_carrera"]
    class Meta:
             model=Carrera
class adminCiclo(admin.ModelAdmin):
    list_display = ["nombre_ciclo"]
    class Meta:
             model=Ciclo
class adminEvento(admin.ModelAdmin):
    list_display = ["nombre_evento"]
    class Meta:
             model=Evento
class adminNota(admin.ModelAdmin):
    list_display = ["nombre_estudiante","docente","nombre_carrera","nombre_asignatura","nombre_evento","semeunicodee","semestre"]
    class Meta:
             model=Nota
class adminPersona(admin.ModelAdmin):
    list_display = ["nombre_persona","alias_persona","contraseña_persona","telefono_persona","direccion_persona","email_persona"]
    class Meta:
             model=Persona
admin.site.register(Nota, adminNota)
admin.site.register(Asistencia, adminAsistencia)
admin.site.register(Persona, adminPersona)
admin.site.register(Carrera, adminCarrera)
admin.site.register(Ciclo, adminCiclo)
admin.site.register(Docente)
admin.site.register(Evento, adminEvento)
admin.site.register(Asignatura, adminAsignatura)
admin.site.register(Estudiante)