from django.apps import AppConfig


class NotasasistenciaConfig(AppConfig):
    name = 'NotasAsistencia'
