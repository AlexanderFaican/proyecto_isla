from django.db import models
# Create your models here.
class Persona(models.Model):
    id_persona=models.AutoField(primary_key=True)
    nombre_persona = models.CharField(max_length=60, verbose_name='Apellidos y Nombres')
    alias_persona = models.CharField(max_length=30, unique=True)
    contraseña_persona = models.CharField(max_length=30, null=False, verbose_name='Contraseña')
    telefono_persona = models.IntegerField(verbose_name='Teléfono', null=False)
    direccion_persona = models.CharField(verbose_name='Dirección',max_length=100, null=False)
    email_persona = models.EmailField(verbose_name='E-mail', null=False)
    def __unicode__(self):
        return self.nombre_persona
class Estudiante(models.Model):
    id_estudiante = models.AutoField(primary_key=True)
    nombre_estudiante = models.OneToOneField(Persona, on_delete=models.CASCADE, verbose_name='Estudiante')
    def __unicode__(self):
        return self.nombre_cliente
class Asignatura(models.Model):
    id_asignatura=models.AutoField(primary_key=True)
    codigo_asignatura=models.CharField(max_length=6, null=False, verbose_name='Código')
    nombre_asignatura=models.CharField(max_length=60, null=False, verbose_name='Nombre de la Asignatura')
    def __unicode__(self):
        return self.codigo_asignatura 
class Docente(models.Model):
    id_docente=models.AutoField(primary_key=True)
    nombre_docente = models.OneToOneField(Persona, on_delete=models.CASCADE,verbose_name='Docente')
    def __unicode__(self):
        return self.nombre_docente

class Asistencia(models.Model):
    id_persona = models.AutoField(primary_key=True)
    nombre_asignatura = models.OneToOneField(Asignatura, on_delete=models.CASCADE, verbose_name='Asignatura',default='')
    nombre_estudiante = models.OneToOneField( Estudiante, on_delete=models.CASCADE,verbose_name='Estudiante',default='')
    nombre_docente = models.OneToOneField(Docente, on_delete=models.CASCADE,verbose_name='Docente',default='')
    numero_horas = models.IntegerField(null=False, verbose_name='Número de Horas',default=0)
    numero_creditos = models.IntegerField(null=False, verbose_name='Número de Créditos',default=0)
    total_asistencias = models.CharField( null=False,max_length=4,verbose_name='Total de Asistencias',default='')
    total_inasistencia = models.CharField( null=False,max_length=4,verbose_name='Total de Inasistencias',default='')
    def __unicode__(self):
        return self.nombre_asignatura

class Carrera(models.Model):
    id_carrera=models.AutoField(primary_key=True)
    codigo_carrera=models.CharField(max_length=4, null=False, verbose_name='Código')
    nombre_carrera=models.CharField(max_length=50, null=False, verbose_name='Nombre de la Carrera')
    def __unicode__(self):
        return self.codigo_carrera
class Ciclo(models.Model):
    id_ciclo = models.AutoField(primary_key=True)
    nombre_ciclo = models.CharField(max_length=30, null=False, verbose_name='Nombre del Ciclo')
    #nombre_ciclo = models.OneToOneField(Asignatura, on_delete=models.CASCADE, verbose_name='Ciclo')
    def __unicode__(self):
        return self.nombre_ciclo
class Evento(models.Model):
    id_evento = models.AutoField(primary_key=True)
    nombre_evento = models.CharField(max_length=30, null=False, verbose_name='Nombre del Evento')
    #nombre_evento=models.OneToOneField(Ciclo, on_delete=models.CASCADE, verbose_name="Evento")
    def __unicode__(self):
        return self.nombre_evento
class Nota(models.Model):
    id_notas = models.AutoField(primary_key=True)
    nombre_estudiante = models.OneToOneField( Estudiante, on_delete=models.CASCADE,verbose_name='Estudiante')
    docente = models.OneToOneField( Docente, on_delete=models.CASCADE,verbose_name='Docente')
    nombre_carrera = models.OneToOneField(Carrera, on_delete=models.CASCADE, verbose_name='Carrera')
    nombre_asignatura = models.OneToOneField(Asignatura, on_delete=models.CASCADE, verbose_name='Asignatura')
    nombre_evento = models.OneToOneField(Evento, on_delete=models.CASCADE, verbose_name='Evento')
    semeunicodee = models.IntegerField(verbose_name='Semiunicode', null=False)
    semestre = models.IntegerField(verbose_name='Nota',null=False,default=0)



    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.docente 