
from django.shortcuts import render, redirect
from django.shortcuts import render
from apps.NotasAsistencia.models import Estudiante, Evento, Nota,Asistencia, Docente, Asignatura,Carrera,Persona

def gestion_view(request):
    listaNotas = Nota.objects.all()
    listaAsistencias = Asistencia.objects.all()
    listaPersonas = Persona.objects.all()
    return render(request,"gestion_a.html",locals())

def editarNota(request,id):
    if request.method =='POST':

        nota = Nota.objects.get(id_notas = id)
        estudiante = Persona.objects.get( id_persona = request.POST['estudiante'])
        docente = Persona.objects.get(id_persona = request.POST['docente'])
         
        nota.nombre_asignatura = Asignatura.objects.get(id_asignatura = request.POST['asignatura'])
        nota.nombre_evento = Evento.objects.get(id_evento = request.POST['evento'] )
        nota.nombre_estudiante = Estudiante.objects.get(nombre_estudiante = estudiante)
        nota.docente = Docente.objects.get(nombre_docente = docente)
        nota.nombre_carrera = Carrera.objects.get(id_carrera = request.POST['carrera'])
        
        nota.semeunicodee = request.POST['semeunicodee']
        nota.semestre = request.POST['semestre']

        nota.save()

        return redirect(gestion_view)
    else:
        nota = Nota.objects.get(id_notas = id)
        print(nota.nombre_estudiante.nombre_estudiante.id_persona)
        estudiantes = Estudiante.objects.all()
        docentes = Docente.objects.all()
        carreras = Carrera.objects.all()
        asignaturas = Asignatura.objects.all()
        eventos = Evento.objects.all()

        return render(request,"editarNota.html",locals())

# Create your views here.
