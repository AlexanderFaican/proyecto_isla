from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
from .views import gestion_view, editarNota
urlpatterns = [
    path('gestion',gestion_view, name='gestion'),
    path('editarnota/<int:id>',editarNota, name='editarnota'),
    ] 