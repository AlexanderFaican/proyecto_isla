from django.db import models
from datetime import datetime


class Carrera(models.Model):
    id_carrera = models.AutoField(primary_key=True)
    nombre_carrera = models.CharField(verbose_name='Nombre Carrera',max_length=20, null=False)
    codigo_carrera = models.CharField(verbose_name='Código Carrera',max_length=4, null= False)


    def __unicode__(self):
        return self.nombre_carrera

class Asignatura(models.Model):
    id_asignatura = models.AutoField(primary_key=True)
    nombre_asignatura = models.CharField(verbose_name='Nombre Asignatura',max_length=20, null= False)
    codigo_asignatura= models.CharField(verbose_name='Código Asignatura',max_length=6, null= False)
 

    def __unicode__(self):
        return self.nombre_asignatura


class Docente(models.Model):
    id_docente = models.AutoField(primary_key=True)
    nombre_docente= models.CharField(verbose_name='Nombre Docente',max_length=30, null= False)
    email_docente = models.EmailField(verbose_name='Email del Docente',max_length=30,null=False)


    def __unicode__(self):
        return self.nombre_docente

class Ciclo(models.Model):
    id_ciclo = models.AutoField(primary_key=True)
    nombre_ciclo= models.CharField(verbose_name='Nombre del Ciclo',max_length=30, null= False)


    def __unicode__(self):
        return self.nombre_ciclo

class Seccion(models.Model):
    id_seccion = models.AutoField(primary_key=True)
    nombre_seccion = models.CharField(verbose_name='Nombre de la Sección',max_length=30, null= False)


    def __unicode__(self):
        return self.nombre_seccion

class Evento(models.Model):
    id_evento = models.AutoField(primary_key=True)
    nombre_evento = models.CharField(verbose_name='Nombre del Evento',max_length=30, null= False)


    def __unicode__(self):
        return self.id_nombre_evento


class PlandeClasesDiarioSemanal(models.Model):

    id_plandeclasediariosemanal = models.AutoField(primary_key=True)

    carrera = models.OneToOneField(Carrera, on_delete=models.CASCADE, verbose_name='CARRERA')
    asignatura = models.OneToOneField(Asignatura, on_delete=models.CASCADE, verbose_name='ASIGNATURA')
    fecha_de_inicio_del_evento = models.DateTimeField(verbose_name='FECHA DE INICIO DEL EVENTO',default=datetime.now(),null=True, blank=True)
    docentec = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='DOCENTE')
    codigo = models.CharField(verbose_name='CÓDIGO',max_length=70,null=False)
    ciclo = models.OneToOneField(Ciclo, on_delete=models.CASCADE, verbose_name='CICLO')
    seccion = models.OneToOneField(Seccion, on_delete=models.CASCADE, verbose_name='SECCIÓN')
    fecha_de_finalizacion = models.DateTimeField(verbose_name='FECHA DE FINALIZACIÓN',default=datetime.now(), null=True, blank=True)
    fecha_de_subida = models.DateTimeField(verbose_name='FECHA DE SUBIDA',default=datetime.now(), null=True, blank=True)
    semana1 = models.CharField(verbose_name='SEMANA 1',max_length=70,null=False)

    fecha = models.DateTimeField(verbose_name='FECHA',default=datetime.now(), null=True, blank=True)
    resumen = models.CharField(verbose_name='RESUMEN DE LOS TEMAS A TRATAR',max_length=200,null=False)
    actividades = models.TextField(verbose_name='ACTIVIDADES A REALIZARSE',max_length=500, null=False)
    recursos = models.CharField(verbose_name='RECURSOS A EMPLEARSE',max_length=100,null=False)
    tarea = models.CharField(verbose_name='TAREA A ENVIAR',max_length=100,null=False)
    horas = models.IntegerField(verbose_name='NRO_HORAS',default="0")
    resultado = models.CharField(verbose_name='RESULTADOS DE APRENDIZAJE',max_length=100,null=False)

    firma_elaborado = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #nombre_elaborado = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='NOMBRE')
    fecha_elaborado = models.DateTimeField(verbose_name='FECHA',default=datetime.now(), null=True, blank=True)

    firma_revisado = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #nombre_revisado = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='DOCENTE')
    fecha_revisado = models.DateTimeField(verbose_name='FECHA',default=datetime.now(), null=True, blank=True)

    firma_aprobado = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #nombre_aprobado = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='NOMBRE')
    fecha_aprobado = models.DateTimeField(verbose_name='FECHA',default=datetime.now(), null=True, blank=True)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.carrera

class ControlAcademico (models.Model):

    id_controlacademico = models.AutoField(primary_key=True)

    carrera_control = models.OneToOneField(Carrera, on_delete=models.CASCADE, verbose_name='CARRERA')
    asignatura_control = models.OneToOneField(Asignatura, on_delete=models.CASCADE, verbose_name='ASIGNATURA')
    docente_control = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='DOCENTE')
    periodo_academico_control = models.CharField(verbose_name='PERIODO ACADEMICO',max_length=70,null=False)
    ciclo_control = models.OneToOneField(Ciclo, on_delete=models.CASCADE, verbose_name='CICLO')
    evento_control = models.OneToOneField(Evento, on_delete=models.CASCADE, verbose_name='EVENTO')
    codigo_control = models.CharField(verbose_name='CÓDIGO',max_length=70,null=False)
    seccion_control = models.OneToOneField(Seccion, on_delete=models.CASCADE, verbose_name='SECCIÓN')

    fecha_control = models.DateTimeField(verbose_name='fecha (año-mes-día)',default=datetime.now(), null=True, blank=True)
    tema_control = models.CharField(verbose_name='Tema Dictado',max_length=200,null=False)
    actividades_control = models.CharField(verbose_name='Actividades',max_length=100,null=False)
    recursos_control = models.CharField(verbose_name='Recursos',max_length=100,null=False)
    firma_control = models.CharField(verbose_name='Firma Delegado Estudiantil',max_length=100,null=False)

    firma_elaborado_control = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #nombre_elaborado_control = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='NOMBRE')
    fecha_elaborado_control = models.DateTimeField(verbose_name='FECHA',default=datetime.now(), null=True, blank=True)

    firma_coordinador_control = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #nombre_coordinador_control = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='NOMBRE')
    fecha_coordinador_control = models.DateTimeField(verbose_name='FECHA',default=datetime.now(), null=True, blank=True)

    firma_vicerrector_control = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #nombre_vicerrector_control = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='NOMBRE')
    fecha_vicerrector_control = models.DateTimeField(verbose_name='FECHA',default=datetime.now(), null=True, blank=True)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.carrera_control