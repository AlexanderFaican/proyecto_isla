from django.apps import AppConfig


class PlanclasesConfig(AppConfig):
    name = 'PlanClases'
