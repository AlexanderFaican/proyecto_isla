from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
from .views import plan_view,silabo_view,editarAsignatura, editarCarrera, editarDocente
urlpatterns = [
    path('planclases/',plan_view, name='planClases'),
    path('silabo/',silabo_view, name='silabo'),
    path('editarasignatura/<int:id>',editarAsignatura, name='editarasignatura'),
    path('editarcarrera/<int:id>',editarCarrera, name='editarcarrera'),
    path('editardocente/<int:id>',editarDocente, name='editardocente'),

    ] 