from django.shortcuts import render, redirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from apps.NotasAsistencia.models import Estudiante, Evento, Nota,Asistencia, Asignatura,Persona
from apps.PlanClases.models import  Carrera, Asignatura, Docente
@login_required 
def plan_view(request):
    docentes = Docente.objects.all()
    carreras = Carrera.objects.all()
    asignaturas = Asignatura.objects.all()
    print(docentes)
    print(carreras)
    print(asignaturas)
    return render(request,"planClases.html",locals())
@login_required  
def silabo_view(request):
    docentes = Docente.objects.all()
    carreras = Carrera.objects.all()
    asignaturas = Asignatura.objects.all()
    print(docentes)
    print(carreras)
    print(asignaturas)
    return render(request,"Silabo.html",locals())
def editarAsignatura(request,id):
    asignatura = Asignatura.objects.get(id_asignatura = id)
    if request.method =='POST':

        
        asignatura.nombre_asignatura = request.POST['nombre_asignatura']
        asignatura.codigo_asignatura = request.POST['codigo_asignatura']

        asignatura.save()

        return redirect(plan_view)
    else:

        return render(request,"editarAsignatura.html",locals())
def editarCarrera(request,id):
    carrera = Carrera.objects.get(id_carrera = id)
    if request.method =='POST':

        
        carrera.nombre_carrera = request.POST['nombre_carrera']
        carrera.codigo_carrera = request.POST['codigo_carrera']

        carrera.save()

        return redirect(plan_view)
    else:

        return render(request,"editarCarrera.html",locals())
def editarDocente(request,id):
    docente = Docente.objects.get(id_docente = id)
    if request.method =='POST':

        
        docente.nombre_docente = request.POST['nombre_docente']
        docente.email_docente = request.POST['email_docente']

        docente.save()

        return redirect(plan_view)
    else:

        return render(request,"editarDocente.html",locals())
