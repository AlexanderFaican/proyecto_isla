from django.contrib import admin

from .models import PlandeClasesDiarioSemanal
from .models import ControlAcademico
from .models import Carrera
from .models import Asignatura
from .models import Docente
from .models import Ciclo
from .models import Seccion
from .models import Evento

# Register your models here.
admin.site.register(PlandeClasesDiarioSemanal)
admin.site.register(Carrera)
admin.site.register(Asignatura)
admin.site.register(Docente)
admin.site.register(Ciclo)
admin.site.register(Seccion)
admin.site.register(ControlAcademico)
admin.site.register(Evento)