from django.contrib import admin

from .models import InformacionGeneral
from .models import PrerrequisitosyCorrequisitos
from .models import DescripciondelaAsignatura
from .models import Objetivosespecificosdelaasignatura
from .models import ResultudosdeAprendizajesdelaAsignatura
from .models import CompetenciasGenericasdelaASignatura
from .models import ComponentesEspecificasdelaAsignatura
from .models import UnidadesCurriculares
from .models import DesarrolloSemanaldelaAsignatura
from .models import CriteriodeevaluaciondelEstudianteporResultadosdeAprendizaje
from .models import Bibliografia
from .models import PerfildelProfesordelaAsignatura
from .models import HabilidadesquePoseeelDocente
from .models import ActitudesdelDocente
from .models import RelaciondelosCOntenidosdelaAsignaturaconlosResultadosdeAprendizaje
from .models import RelaciondelaAsignaturaconlosResultadosdeAprendizajedelPerfildeEgresodelaCarrera
from .models import RevisionyAprovacion
from .models import ActaEntrega
from .models import Carrera
from .models import Asignatura
from .models import Evento
from .models import Docente
from .models import Ciclo
from .models import Seccion
# Register your models here.





admin.site.register(InformacionGeneral)
admin.site.register(PrerrequisitosyCorrequisitos)
admin.site.register(DescripciondelaAsignatura)
admin.site.register(Objetivosespecificosdelaasignatura)
admin.site.register(ResultudosdeAprendizajesdelaAsignatura)
admin.site.register(CompetenciasGenericasdelaASignatura)
admin.site.register(ComponentesEspecificasdelaAsignatura)
admin.site.register(UnidadesCurriculares)
admin.site.register(DesarrolloSemanaldelaAsignatura)
admin.site.register(CriteriodeevaluaciondelEstudianteporResultadosdeAprendizaje)
admin.site.register(Bibliografia)
admin.site.register(PerfildelProfesordelaAsignatura)
admin.site.register(HabilidadesquePoseeelDocente)
admin.site.register(ActitudesdelDocente)
admin.site.register(RelaciondelosCOntenidosdelaAsignaturaconlosResultadosdeAprendizaje)
admin.site.register(RelaciondelaAsignaturaconlosResultadosdeAprendizajedelPerfildeEgresodelaCarrera)
admin.site.register(RevisionyAprovacion)
admin.site.register(ActaEntrega)
admin.site.register(Carrera)
admin.site.register(Asignatura)
admin.site.register(Evento)
admin.site.register(Docente)
admin.site.register(Ciclo)
admin.site.register(Seccion)