from django.db import models
from datetime import datetime


class Carrera(models.Model):
    id_carrera = models.AutoField(primary_key=True)
    nombre_carrera = models.CharField(verbose_name='Nombre Carrera',max_length=20, null=False)
    codigo_carrera = models.CharField(verbose_name='Código Carrera',max_length=4, null= False)


    def __unicode__(self):
        return self.nombre_carrera

class Asignatura(models.Model):
    id_asignatura = models.AutoField(primary_key=True)
    nombre_asignatura = models.CharField(verbose_name='Nombre Asignatura',max_length=20, null= False)
    codigo_asignatura= models.CharField(verbose_name='Código Asignatura',max_length=6, null= False)


    def __unicode__(self):
        return self.nombre_asignatura

class Evento(models.Model):
    id_evento = models.AutoField(primary_key=True)
    nombre_evento = models.CharField(verbose_name='Nombre del Evento',max_length=30, null= False)


    def __unicode__(self):
        return self.nombre_evento

class Docente(models.Model):
    id_docente = models.AutoField(primary_key=True)
    nombre_docente= models.CharField(verbose_name='Nombre Docente',max_length=30, null= False)
    email_docente = models.EmailField(verbose_name='Email del Docente',max_length=30,null=False)


    def __unicode__(self):
        return self.nombre_docente

class Ciclo(models.Model):
    id_ciclo = models.AutoField(primary_key=True)
    nombre_ciclo= models.CharField(verbose_name='Nombre del Ciclo',max_length=30, null= False)


    def __unicode__(self):
        return self.nombre_docente

class Seccion(models.Model):
    id_seccion = models.AutoField(primary_key=True)
    nombre_seccion = models.CharField(verbose_name='Nombre de la Sección',max_length=30, null= False)


    def __unicode__(self):
        return self.nombre_seccion

class InformacionGeneral(models.Model):

    listadounidaddeorganizacioncurricular = (
        ('básica', 'Básica'),
        ('profesional', 'Profesional'),
        ('titulación', 'Titulación')
    )

    id_informaciongeneral = models.AutoField(primary_key=True)
    carrera_informacion = models.OneToOneField(Carrera, on_delete=models.CASCADE, verbose_name='Carrera')
    #asignatura_informacion = models.OneToOneField(Asignatura, on_delete=models.CASCADE, verbose_name='Asignatura')
    periodo_academico = models.CharField(verbose_name='Período Académico',max_length=70,null=False)
    evento = models.OneToOneField(Evento, on_delete=models.CASCADE, verbose_name='Evento')
    nivel = models.CharField(verbose_name='Nivel',max_length=70,null=False)
    horas_de_clase_por_el_periodo = models.IntegerField(verbose_name='Horas de clase por el Período')
    profesor_de_la_asignatura = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='Profesor de la Asignatura')
    unidad_de_organizacion_curricular = models.CharField(verbose_name='Unidad de Organización Curricular',max_length=30, choices=listadounidaddeorganizacioncurricular,null=False, default="Básica")
    codigo_de_asignatura = models.OneToOneField(Asignatura, on_delete=models.CASCADE, verbose_name='Código de Asignatura')
    semestre = models.OneToOneField(Ciclo, on_delete=models.CASCADE, verbose_name='Ciclo')
    seccion = models.OneToOneField(Seccion, on_delete=models.CASCADE, verbose_name='Sección')
    numero_de_creditos = models.IntegerField(verbose_name='Número de Créditos')
    fecha_de_elaboracion = models.DateTimeField(verbose_name='Fecha de elaboración',default=datetime.now(), null=True, blank=True)
    fecha_de_subida = models.DateTimeField(verbose_name='Fecha',default=datetime.now(), null=True, blank=True)
    #email_del_profesor_de_la_asignatura = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='Emaildel profesor de la asignatura')

    horas_teoricas_asistidas_por_el_docente = models.IntegerField(verbose_name='HORAS TEÓRICAS ASISTIDAS POR EL DOCENTE (15%)',default="0")
    horas_practicas_asistidas_por_el_docente = models.IntegerField(verbose_name='HORAS PRÁCTICAS ASISTIDAS POR EL DOCENTE (25%)',default="0")
    virtual = models.IntegerField(verbose_name='VIRTUAL (5%)',default="0")
    presencial = models.IntegerField(verbose_name='PRESENCIA (5%)',default="0")
    horas_practicas_de_campo_talleres = models.IntegerField(verbose_name='HORAS DE PRÁCTICAS DE CAMPO, TALLERES (30%)',default="0")
    horas_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE TRABAJO AUTONOMO (20%)',default="0")
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.carrera_informacion

class PrerrequisitosyCorrequisitos(models.Model):

    id_prerrequisitosycorrequisitos = models.AutoField(primary_key=True)
    prerrequisitos_asignatura = models.CharField(verbose_name='Asignatura',max_length=70,null=False)
    prerrequisitos_codigo = models.CharField(verbose_name='Código',max_length=70,null=False)
    correquisitos_asignatura = models.CharField(verbose_name='Asignatura',max_length=70,null=False)
    correquisitos_codigo = models.CharField(verbose_name='Código',max_length=70,null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __unicode__(self):
        return self.prerrequisitos_asignatura

class DescripciondelaAsignatura(models.Model):

    id_descripciondelaasignatura = models.AutoField(primary_key=True)
    descripcion_asignatura = models.TextField(verbose_name='Descripción de la Asignatura',max_length=500, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.descripcion_asignatura

class Objetivosespecificosdelaasignatura(models.Model):

    id_objetivosespecificosdelaasignatura = models.AutoField(primary_key=True)
    descripcion_objetivos = models.TextField(verbose_name='Objetivos específicos de la Asignatura',max_length=500, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.descripcion_objetivos

class ResultudosdeAprendizajesdelaAsignatura(models.Model):
    id_resultadosdeaprendizajedelaasignatura = models.AutoField(primary_key=True)
    descripcion_resultados = models.TextField(verbose_name='Resultados de Aprendizaje de la Asignatura',max_length=500, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.descripcion_resultados

class CompetenciasGenericasdelaASignatura(models.Model):
    id_competenciasgenericasdelaasignatura = models.AutoField(primary_key=True)
    descripcion_competenciasg = models.TextField (verbose_name='Competencias Genéricas de la Asignatura',max_length=500, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.descripcion_competenciasg

class ComponentesEspecificasdelaAsignatura(models.Model):
    id_competenciasespecificasdelaasignatura = models.AutoField(primary_key=True)
    cognoscitivos = models.TextField (verbose_name='Cognoscitivos',max_length=500, null=False)
    procedimentales = models.TextField (verbose_name='Procedimentales',max_length=500, null=False)
    actitudinales = models.TextField (verbose_name='Actitudinales',max_length=500, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.cognoscitivos

class UnidadesCurriculares(models.Model):

    id_unidadescurricularesu1 = models.AutoField(primary_key=True)

    nombre_de_la_unidadu1 = models.CharField(verbose_name='NOMBRE DE LA UNIDAD',max_length=70,null=False)
    resultado_de_aprendizaje_de_la_unidadu1 = models.CharField(verbose_name='RESULTADO DE APRENDIZAJE DE LA UNIDAD',max_length=200, null=False)
    resultado_de_aprendizaje_de_la_unidadu11 = models.CharField(verbose_name='RESULTADO DE APRENDIZAJE DE LA UNIDAD',max_length=200, null=False)
    resultado_de_aprendizaje_de_la_unidadu12 = models.CharField(verbose_name='RESULTADO DE APRENDIZAJE DE LA UNIDAD', max_length=200, null=False)
    resultado_de_aprendizaje_de_la_unidadu13 = models.CharField(verbose_name='RESULTADO DE APRENDIZAJE DE LA UNIDAD', max_length=200, null=False)
    tema1 = models.CharField(verbose_name='TEMA',max_length=120,null=False,default="TEMA")
    tipos_teorica = models.IntegerField(verbose_name='TEÓRICAS',default="0")
    tipos_practicas = models.IntegerField(verbose_name='PRÁCTICAS',default="0")
    tipos_horas_de_tutoria = models.IntegerField(verbose_name='HORAS DE TUTORÍA',default="0")
    tipos_horas_de_actividades_de_prticas = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    tipos_horas_de_actividades_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)',default="0")
    tipos_mecanismo_de_evaluacion = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN',max_length=120,null=False,default="")
    tema2 = models.CharField(verbose_name='TEMA', max_length=120, null=False,default="TEMA")
    porcentaje_teorica = models.IntegerField(verbose_name='TEÓRICAS',default="0")
    porcentaje_practicas = models.IntegerField(verbose_name='PRÁCTICAS',default="0")
    porcentaje_horas_de_tutoria = models.IntegerField(verbose_name='HORAS DE TUTORÍA',default="0")
    porcentaje_horas_de_actividades_de_prticas = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    porcentaje_horas_de_actividades_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)',default="0")
    porcentaje_mecanismo_de_evaluacion = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN',max_length=120, null=False,default="")
    tema3 = models.CharField(verbose_name='TEMA', max_length=120, null=False,default="TEMA")
    exponentes_teorica = models.IntegerField(verbose_name='TEÓRICAS',default="0")
    exponentes_practicas = models.IntegerField(verbose_name='PRÁCTICAS',default="0")
    exponentes_horas_de_tutoria = models.IntegerField(verbose_name='HORAS DE TUTORÍA',default="0")
    exponentes_horas_de_actividades_de_prticas = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    exponentes_horas_de_actividades_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)',default="0")
    exponentes_mecanismo_de_evaluacion = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN',max_length=120, null=False,default="")
    tema4 = models.CharField(verbose_name='TEMA', max_length=120, null=False,default="TEMA")
    regla_teorica = models.IntegerField(verbose_name='TEÓRICAS',default="0")
    regla_practicas = models.IntegerField(verbose_name='PRÁCTICAS',default="0")
    regla_horas_de_tutoria = models.IntegerField(verbose_name='HORAS DE TUTORÍA',default="0")
    regla_horas_de_actividades_de_prticas = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    regla_horas_de_actividades_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)',default="0")
    regla_mecanismo_de_evaluacion = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN',max_length=120, null=False,default="")
    tema5 = models.CharField(verbose_name='TEMA', max_length=120, null=False,default="TEMA")
    evaluacion_teorica = models.IntegerField(verbose_name='TEÓRICAS',default="0")
    evaluacion_practicas = models.IntegerField(verbose_name='PRÁCTICAS',default="0")
    evaluacion_horas_de_tutoria = models.IntegerField(verbose_name='HORAS DE TUTORÍA',default="0")
    evaluacion_horas_de_actividades_de_prticas = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    evaluacion_horas_de_actividades_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)',default="0")
    evaluacion_mecanismo_de_evaluacion = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN',max_length=120, null=False,default="")
    total1= models.CharField(verbose_name='TOTAL', max_length=120, null=False,default="Total")
    total_teorica = models.IntegerField(verbose_name='TEÓRICAS',default="0")
    total_practicas = models.IntegerField(verbose_name='PRÁCTICAS',default="0")
    total_horas_de_tutoria = models.IntegerField(verbose_name='HORAS DE TUTORÍA',default="0")
    total_horas_de_actividades_de_prticas = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    total_horas_de_actividades_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)',default="0")
    total_mecanismo_de_evaluacion = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN',max_length=120, null=False,default="")
    metodologia_de_aprendizajeu1 = models.CharField(verbose_name='METODOLOGÍA DE APRENDIZAJE',max_length=300, null=False)
    recursos_didacticosu1 = models.CharField(verbose_name='RECURSOS DIDÁCTICOS',max_length=200, null=False)


    nombre_de_la_unidadu2 = models.CharField(verbose_name='NOMBRE DE LA UNIDAD',max_length=100, null=False)
    resultado_de_aprendizaje_de_la_unidadu2 = models.CharField(verbose_name='RESULTADO DE APRENDIZAJE DE LA UNIDAD',max_length=200, null=False)
    resultado_de_aprendizaje_de_la_unidadu21 = models.CharField(verbose_name='RESULTADO DE APRENDIZAJE DE LA UNIDAD',max_length=200, null=False)
    resultado_de_aprendizaje_de_la_unidadu22 = models.CharField(verbose_name='RESULTADO DE APRENDIZAJE DE LA UNIDAD', max_length=200, null=False)
    resultado_de_aprendizaje_de_la_unidadu23 = models.CharField(verbose_name='RESULTADO DE APRENDIZAJE DE LA UNIDAD', max_length=200, null=False)
    tema6 = models.CharField(verbose_name='TEMA', max_length=120, null=False,default="TEMA")
    ecuaciones_teorica = models.IntegerField(verbose_name='TEÓRICAS',default="0")
    ecuaciones_practicas = models.IntegerField(verbose_name='PRÁCTICAS',default="0")
    ecuaciones_horas_de_tutoria = models.IntegerField(verbose_name='HORAS DE TUTORÍA',default="0")
    ecuaciones_horas_de_actividades_de_prticas = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    ecuaciones_horas_de_actividades_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)',default="0")
    ecuaciones_mecanismo_de_evaluacion = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN',max_length=120, null=False,default="")
    tema7 = models.CharField(verbose_name='TEMA', max_length=120, null=False,default="TEMA")
    inecuaciones_teorica = models.IntegerField(verbose_name='TEÓRICAS',default="0")
    inecuaciones_practicas = models.IntegerField(verbose_name='PRÁCTICAS',default="0")
    inecuaciones_horas_de_tutoria = models.IntegerField(verbose_name='HORAS DE TUTORÍA',default="0")
    inecuaciones_horas_de_actividades_de_prticas = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    inecuaciones_horas_de_actividades_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)',default="0")
    inecuaciones_mecanismo_de_evaluacion = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN',max_length=120, null=False,default="")
    tema8 = models.CharField(verbose_name='TEMA', max_length=120, null=False,default="TEMA")
    sucesiones_teorica = models.IntegerField(verbose_name='TEÓRICAS',default="0")
    sucesiones_practicas = models.IntegerField(verbose_name='PRÁCTICAS',default="0")
    sucesiones_horas_de_tutoria = models.IntegerField(verbose_name='HORAS DE TUTORÍA',default="0")
    sucesiones_horas_de_actividades_de_prticas = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    sucesiones_horas_de_actividades_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)',default="0")
    sucesiones_mecanismo_de_evaluacion = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN',max_length=120, null=False,default="")
    tema9 = models.CharField(verbose_name='TEMA', max_length=120, null=False,default="TEMA")
    evaluacionu2_teorica = models.IntegerField(verbose_name='TEÓRICAS',default="0")
    evaluacionu2_practicas = models.IntegerField(verbose_name='PRÁCTICAS',default="0")
    evaluacionu2_horas_de_tutoria = models.IntegerField(verbose_name='HORAS DE TUTORÍA',default="0")
    evaluacionu2_horas_de_actividades_de_prticas = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    evaluacionu2_horas_de_actividades_de_trabajo_autonomo = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)',default="0")
    evaluacionu2_mecanismo_de_evaluacion = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN',max_length=120, null=False,default="")
    total2 = models.CharField(verbose_name='TOTAL', max_length=120, null=False,default="Total")
    total_teorica2 = models.IntegerField(verbose_name='TEÓRICAS', default="0")
    total_practicas2 = models.IntegerField(verbose_name='PRÁCTICAS', default="0")
    total_horas_de_tutoria2 = models.IntegerField(verbose_name='HORAS DE TUTORÍA', default="0")
    total_horas_de_actividades_de_prticas2 = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE PRÁCTICA (30%)',default="0")
    total_horas_de_actividades_de_trabajo_autonomo2 = models.IntegerField(verbose_name='HORAS DE ACTIVIDADES DE TRABAJO AUTÓNOMO (20%)', default="0")
    total_mecanismo_de_evaluacion2 = models.CharField(verbose_name='MECANISMOS DE EVALUACIÓN', max_length=120, null=False,default="")
    metodologia_de_aprendizajeu2 = models.CharField(verbose_name='METODOLOGÍA DE APRENDIZAJE',max_length=300, null=False)
    recursos_didacticosu2 = models.CharField(verbose_name='RECURSOS DIDÁCTICOS',max_length=200, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self. nombre_de_la_unidadu1

class DesarrolloSemanaldelaAsignatura(models.Model):

    id_desarrollosemanaldelaasignatura = models.AutoField(primary_key=True)
    semana1 = models.CharField(verbose_name='SEMANA 1',max_length=70,null=False)
    horas_de_duracion_de_clases_a_la_semana1 = models.IntegerField(verbose_name='HORAS DE DURACIÓN DE CLASES A LA SEMANA',default="0")
    contenidos_y_actividad_de_estudio_teorico1 = models.CharField(verbose_name='CONTENIDOS Y ACTIVIDADES DE ESTUDIO TEÓRICO',max_length=120, null=False)
    que_actividades_practicas_se_realizaran1 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES PRÁCTICAS DE REALIZARÁN?',max_length=120, null=False)
    que_actividades_trabajo_autonomo_se_realizaran1 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES DE TRABAJO AUTÓNOMO SE REALIZARÁN',max_length=120, null=False)
    en_que_escenario_de_aprendizaje_se_realizaran1 = models.CharField(verbose_name='¿EN QUÉ ESCENARIO DE APRENDIZAJE SE REALIZARÁN?',max_length=120, null=False)

    semana2 = models.CharField(verbose_name='SEMANA 2',max_length=70, null=False)
    horas_de_duracion_de_clases_a_la_semana2 = models.IntegerField(verbose_name='HORAS DE DURACIÓN DE CLASES A LA SEMANA',default="0")
    contenidos_y_actividad_de_estudio_teorico2 = models.CharField(verbose_name='CONTENIDOS Y ACTIVIDADES DE ESTUDIO TEÓRICO',max_length=120, null=False)
    que_actividades_practicas_se_realizaran2 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES PRÁCTICAS DE REALIZARÁN?',max_length=120, null=False)
    que_actividades_trabajo_autonomo_se_realizaran2 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES DE TRABAJO AUTÓNOMO SE REALIZARÁN',max_length=120, null=False)
    en_que_escenario_de_aprendizaje_se_realizaran2 = models.CharField(verbose_name='¿EN QUÉ ESCENARIO DE APRENDIZAJE SE REALIZARÁN?',max_length=120, null=False)

    semana3 = models.CharField(verbose_name='SEMANA 3',max_length=70, null=False)
    horas_de_duracion_de_clases_a_la_semana3 = models.IntegerField(verbose_name='HORAS DE DURACIÓN DE CLASES A LA SEMANA',default="0")
    contenidos_y_actividad_de_estudio_teorico3 = models.CharField(verbose_name='CONTENIDOS Y ACTIVIDADES DE ESTUDIO TEÓRICO',max_length=120, null=False)
    que_actividades_practicas_se_realizaran3 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES PRÁCTICAS DE REALIZARÁN?',max_length=120, null=False)
    que_actividades_trabajo_autonomo_se_realizaran3 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES DE TRABAJO AUTÓNOMO SE REALIZARÁN',max_length=120, null=False)
    en_que_escenario_de_aprendizaje_se_realizaran3 = models.CharField(verbose_name='¿EN QUÉ ESCENARIO DE APRENDIZAJE SE REALIZARÁN?',max_length=120, null=False)

    semana4 = models.CharField(verbose_name='SEMANA 4',max_length=70, null=False)
    horas_de_duracion_de_clases_a_la_semana4 = models.IntegerField(verbose_name='HORAS DE DURACIÓN DE CLASES A LA SEMANA',default="0")
    contenidos_y_actividad_de_estudio_teorico4 = models.CharField(verbose_name='CONTENIDOS Y ACTIVIDADES DE ESTUDIO TEÓRICO',max_length=120, null=False)
    que_actividades_practicas_se_realizaran4 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES PRÁCTICAS DE REALIZARÁN?',max_length=120, null=False)
    que_actividades_trabajo_autonomo_se_realizaran4 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES DE TRABAJO AUTÓNOMO SE REALIZARÁN',max_length=120, null=False)
    en_que_escenario_de_aprendizaje_se_realizaran4 = models.CharField(verbose_name='¿EN QUÉ ESCENARIO DE APRENDIZAJE SE REALIZARÁN?',max_length=120, null=False)

    semana5 = models.CharField(verbose_name='SEMANA 5',max_length=70, null=False)
    horas_de_duracion_de_clases_a_la_semana5 = models.IntegerField(verbose_name='HORAS DE DURACIÓN DE CLASES A LA SEMANA',default="0")
    contenidos_y_actividad_de_estudio_teorico5 = models.CharField(verbose_name='CONTENIDOS Y ACTIVIDADES DE ESTUDIO TEÓRICO',max_length=120, null=False)
    que_actividades_practicas_se_realizaran5 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES PRÁCTICAS DE REALIZARÁN?',max_length=120, null=False)
    que_actividades_trabajo_autonomo_se_realizaran5 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES DE TRABAJO AUTÓNOMO SE REALIZARÁN',max_length=120, null=False)
    en_que_escenario_de_aprendizaje_se_realizaran5 = models.CharField(verbose_name='¿EN QUÉ ESCENARIO DE APRENDIZAJE SE REALIZARÁN?',max_length=120, null=False)

    semana6 = models.CharField(verbose_name='SEMANA 6',max_length=70, null=False)
    horas_de_duracion_de_clases_a_la_semana6 = models.IntegerField(verbose_name='HORAS DE DURACIÓN DE CLASES A LA SEMANA',default="0")
    contenidos_y_actividad_de_estudio_teorico6 = models.CharField(verbose_name='CONTENIDOS Y ACTIVIDADES DE ESTUDIO TEÓRICO',max_length=120, null=False)
    que_actividades_practicas_se_realizaran6 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES PRÁCTICAS DE REALIZARÁN?',max_length=120, null=False)
    que_actividades_trabajo_autonomo_se_realizaran6 = models.CharField(verbose_name='¿QUÉ ACTIVIDADES DE TRABAJO AUTÓNOMO SE REALIZARÁN',max_length=120, null=False)
    en_que_escenario_de_aprendizaje_se_realizaran6 = models.CharField(verbose_name='¿EN QUÉ ESCENARIO DE APRENDIZAJE SE REALIZARÁN?',max_length=120, null=False)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.semana1

class CriteriodeevaluaciondelEstudianteporResultadosdeAprendizaje(models.Model):

    id_criteriodeevaluaciondelestudianteporresultadosdeaprendizaje = models.AutoField(primary_key=True)
    evaluacio_porcentaje1 = models.CharField(verbose_name='Porcentaje',max_length=60, null=False)
    evaluacion_puntos1 = models.CharField(verbose_name='(Puntos)',max_length=60, null=False)
    evaluacion_porcentaje2 = models.CharField(verbose_name='Porcentaje',max_length=60, null=False)
    evaluacion_puntos2 = models.CharField(verbose_name='(Puntos)',max_length=60, null=False)
    trabajosi_porcentaje1 = models.CharField(verbose_name='Porcentaje',max_length=60, null=False)
    trabajosi_puntos1 = models.CharField(verbose_name='(Puntos)',max_length=60, null=False)
    trabajosi_porcentaje2 = models.CharField(verbose_name='Porcentaje',max_length=60, null=False)
    trabajosi_puntos2 = models.CharField(verbose_name='(Puntos)',max_length=60, null=False)
    trabajosau_porcentaje1 = models.CharField(verbose_name='Porcentaje',max_length=60, null=False)
    trabajosau_puntos1 = models.CharField(verbose_name='(Puntos)',max_length=60, null=False)
    trabajosau_porcentaje2 = models.CharField(verbose_name='Porcentaje',max_length=60, null=False)
    trabajosau_puntos2 = models.CharField(verbose_name='(Puntos)',max_length=60, null=False)
    trabajosin_porcentaje1 = models.CharField(verbose_name='Porcentaje',max_length=60, null=False)
    trabajosin_puntos1 = models.CharField(verbose_name='(Puntos)',max_length=60, null=False)
    trabajosin_porcentaje2 = models.CharField(verbose_name='Porcentaje',max_length=60, null=False)
    trabajosin_puntos2 = models.CharField(verbose_name='(Puntos)',max_length=60, null=False)
    total_porcentaje1 = models.CharField(verbose_name='Porcentaje',max_length=60, null=False)
    total_puntos1 = models.CharField(verbose_name='(Puntos)',max_length=60, null=False)
    total_porcentaje2 = models.CharField(verbose_name='Porcentaje',max_length=60, null=False)
    total_puntos2 = models.CharField(verbose_name='(Puntos)',max_length=60, null=False)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.evaluacio_porcentaje1

class Bibliografia(models.Model):
    id_bibliografia = models.AutoField(primary_key=True)

    basica_fisica_autor = models.CharField(verbose_name='AUTOR',max_length=70, null=False)
    basica_fisica_titulo_del_libro= models.CharField(verbose_name='TÍTULO DEL LIBRO',max_length=70, null=False)
    basica_fisica_ciudad_de_publicacion = models.CharField(verbose_name='CIUDAD, PAÍS DE PUBLICACIÓN',max_length=70, null=False)
    basica_fisica_edicion = models.CharField(verbose_name='EDICIÓN',max_length=70, null=False)
    basica_fisica_año_de_publicacion = models.IntegerField(verbose_name='AÑO DE PUBLICACIÓN',default="0")
    basica_fisica_editorial= models.CharField(verbose_name='EDITORIAL',max_length=70, null=False)
    basica_fisica_ISBM = models.CharField(verbose_name='ISBN',max_length=70, null=False)

    basica_virtual_autor = models.CharField(verbose_name='AUTOR',max_length=70, null=False)
    basica_virtual_titulo_del_libro = models.CharField(verbose_name='TÍTULO DEL LIBRO',max_length=70, null=False)
    basica_virtual_direccion_electronica = models.CharField(verbose_name='DIRECCIÓN ELECTRÓNICA',max_length=200, null=False)
    basica_virtual_año_de_publicacion = models.IntegerField(verbose_name='AÑO DE PUBLICACIÓN',default="0")
    basica_virtual_editorial = models.CharField(verbose_name='EDITORIAL',max_length=70, null=False)
    basica_virtual_ISBM = models.CharField(verbose_name='ISBN/ISSN',max_length=70, null=False)

    complementaria_fisica_autor = models.CharField(verbose_name='AUTOR',max_length=70, null=False)
    complementaria_fisica_titulo_del_libro = models.CharField(verbose_name='TÍTULO DEL LIBRO',max_length=70, null=False)
    complementaria_fisica_ciudad_de_publicacion = models.CharField(verbose_name='CIUDAD, PAÍS DE PUBLICACIÓN',max_length=70, null=False)
    complementaria_fisica_edicion = models.CharField(verbose_name='EDICIÓN',max_length=70, null=False)
    complementaria_fisica_año_de_publicacion = models.IntegerField(verbose_name='AÑO DE PUBLICACIÓN',default="0")
    complementaria_fisica_editorial = models.CharField(verbose_name='EDITORIAL',max_length=70, null=False)
    complementaria_fisica_ISBM = models.CharField(verbose_name='ISBN',max_length=70, null=False)

    complementaria_virtual_autor = models.CharField(verbose_name='AUTOR',max_length=70, null=False)
    complementaria_virtual_titulo_del_libro = models.CharField(verbose_name='TÍTULO DEL LIBRO',max_length=70, null=False)
    complementaria_virtual_direccion_electronica = models.CharField(verbose_name='DIRECCIÓN ELECTRÓNICA',max_length=200, null=False)
    complementaria_virtual_año_de_publicacion = models.IntegerField(verbose_name='AÑO DE PUBLICACIÓN',default="0")
    complementaria_virtual_editorial = models.CharField(verbose_name='EDITORIAL',max_length=70, null=False)
    complementaria_virtual_ISBM = models.CharField(verbose_name='ISBN/ISSN',max_length=70, null=False)

    complementaria_recursos_autor = models.CharField(verbose_name='AUTOR',max_length=70, null=False)
    complementaria_recursos_titulo_del_libro = models.CharField(verbose_name='TÍTULO DEL LIBRO',max_length=70, null=False)
    complementaria_recursos_ciudad_de_publicacion = models.CharField(verbose_name='CIUDAD, PAÍS DE PUBLICACIÓN',max_length=70, null=False)
    complementaria_recursos_año_de_publicacion = models.IntegerField(verbose_name='AÑO DE PUBLICACIÓN',default="0")
    complementaria_recursos_direccion_electronica = models.CharField(verbose_name='DIRECCIÓN ELECTRÓNICA',max_length=200, null=False)
    complementaria_recursos_ISBM = models.CharField(verbose_name='ISBN',max_length=70, null=False)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.basica_fisica_autor

class PerfildelProfesordelaAsignatura (models.Model):

    id_perdildelprofesordelaasignatura = models.AutoField(primary_key=True)
    dtitulo = models.CharField(verbose_name='TÍTULO(S)',max_length=70, null=False)
    despecialidad = models.CharField(verbose_name='ESPECIALIDAD/MENCIÓN',max_length=70, null=False)
    dnivel = models.CharField(verbose_name='NIVEL',max_length=70, null=False)
    dregistro_en_la_senecyt = models.CharField(verbose_name='REGISTRO EN LA SENECYT',max_length=70, null=False)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.dtitulo

class HabilidadesquePoseeelDocente (models.Model):

    id_habilidadesqueposeeeldocente = models.AutoField(primary_key=True)
    descripcion_habilidades = models.TextField(verbose_name='HABILIDADES QUE POSEE EL DOCENTE',max_length=300, null=False)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.descripcion_habilidades

class ActitudesdelDocente (models.Model):

    id_actitudesdeldocente = models.AutoField(primary_key=True)
    descripcion_actitudes = models.TextField(verbose_name='ACTITUDES DEL DOCENTE',max_length=300, null=False)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.descripcion_actitudes

class RelaciondelosCOntenidosdelaAsignaturaconlosResultadosdeAprendizaje(models.Model):

    listadocontribucion= (
        ('alta', 'Alta'),
        ('media', 'Media'),
        ('baja', 'Baja')
    )

    id_relaciondeloscontenidosdelaasignaturaconlosresultadosdeaprendizaje = models.AutoField(primary_key=True)
    tipo_de_numero_y_sus_propiedades = models.CharField(verbose_name='Tipos de números y sus propiedades',max_length=30, choices=listadocontribucion,null=False, default="Alta")
    porcentaje = models.CharField(verbose_name='Porcentaje',max_length=30, choices=listadocontribucion,null=False, default="Alta")
    exponentes =  models.CharField(verbose_name='Exponentes y sus leyes',max_length=30, choices=listadocontribucion,null=False, default="Alta")
    regla =  models.CharField(verbose_name='REgla de los tres, Probabilidades',max_length=30, choices=listadocontribucion,null=False, default="Alta")
    resultados_tipos = models.TextField(verbose_name='RESULTADO DE APRENDIZAJE',max_length=500, null=False)
    ecuaciones = models.CharField(verbose_name='Ecuaciones, Propiedades de la igualdad, Ecuaciones de primer grado, Ecuaciones cuadrática',max_length=30, choices=listadocontribucion,null=False, default="Alta")
    inecuaciones = models.CharField(verbose_name='Inecuaciones, Propiedades de las desigualdades, Aplicación de las inecuaciones',max_length=30, choices=listadocontribucion,null=False, default="Alta")
    sucesiones = models.CharField(verbose_name='Sucesiones',max_length=30, choices=listadocontribucion,null=False, default="Alta")
    resultados_ecuaciones = models.TextField(verbose_name='RESULTADO DE APRENDIZAJE',max_length=500, null=False)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.tipo_de_numero_y_sus_propiedades

class RelaciondelaAsignaturaconlosResultadosdeAprendizajedelPerfildeEgresodelaCarrera(models.Model):

    listadocontribucion = (
        ('alta', 'Alta'),
        ('media', 'Media'),
        ('baja', 'Baja')
    )

    id_resultadosdelaagisnaturaconlosresultadosdeaprendizajedelperfildeegresodelacarrera = models.AutoField(primary_key=True)
    difine = models.CharField(verbose_name='Define los conceptos y estructura básica',max_length=30, choices=listadocontribucion,null=False, default="Alta")
    explica = models.CharField(verbose_name='RESULTADO DE APRENDIZAJE',max_length=30, choices=listadocontribucion,null=False, default="Alta")
    aplica = models.CharField(max_length=30, choices=listadocontribucion,null=False, default="Alta")
    resultados_define= models.TextField(verbose_name='PERFIL DE EGRESO DE LA CABRERA',max_length=500, null=False)
    definelos = models.CharField(max_length=30, choices=listadocontribucion,null=False, default="Alta")
    analiza = models.CharField(max_length=30, choices=listadocontribucion,null=False, default="Alta")
    resultados_analiza = models.TextField(verbose_name='RESULTADO DE APRENDIZAJE',max_length=500, null=False)
    evaluacion = models.CharField(max_length=30, choices=listadocontribucion,null=False, default="Alta")
    resultados_evaluacion = models.TextField(verbose_name='RESULTADO DE APRENDIZAJE',max_length=500, null=False)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.difine

class RevisionyAprovacion(models.Model):

    id_revisionyaprovacion = models.AutoField(primary_key=True)

    firma_elaborado = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #nombre_elaborado = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='Nombre')
    rol_elaborado = models.CharField(verbose_name='Rol',max_length=70, null=False)
    fecha_elaborado =  models.DateTimeField(verbose_name='Fecha',default=datetime.now(), null=True, blank=True)

    firma_revisado = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #nombre_revisado = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='Nombre')
    rol_revisado  = models.CharField(verbose_name='Rol',max_length=70, null=False)
    fecha_revisado  = models.DateTimeField(verbose_name='Fecha',default=datetime.now(), null=True, blank=True)

    firma_aprovado = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #nombre_aprovado= models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='Nombre')
    rol_aprovado = models.CharField(verbose_name='Rol',max_length=70, null=False)
    fecha_aprovado = models.DateTimeField(verbose_name='Fecha',default=datetime.now(), null=True, blank=True)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.firma_elaborado

class ActaEntrega (models.Model):

    id_actaentrega = models.AutoField(primary_key=True)
    carrera_acta_rega = models.OneToOneField(Carrera, on_delete=models.CASCADE, verbose_name='CARRERA')
    asignatura = models.OneToOneField(Asignatura, on_delete=models.CASCADE, verbose_name='ASIGNATURA')
    docente = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='DOCENTE')
    periodo_academico = models.CharField(verbose_name='PERIODO ACADEMICO',max_length=70,null=False)
    ciclo=  models.OneToOneField(Ciclo, on_delete=models.CASCADE, verbose_name='CICLO')
    evento = models.OneToOneField(Evento, on_delete=models.CASCADE, verbose_name='EVENTO')
    codigo = models.CharField(verbose_name='CÓDIGO',max_length=70,null=False)
    seccion = models.OneToOneField(Seccion, on_delete=models.CASCADE, verbose_name='SECCIÓN')

    numero = models.IntegerField(default="0", verbose_name='No.')
    nombre_del_estudiante = models.CharField(max_length=70, null=False, verbose_name='Nombre del estudiante')
    firma_recepcion = models.CharField(max_length=70, null=False, verbose_name='Firma-Recepción')

    elaborado_firma = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #elaborado_nombre = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='NOMBRE')
    elaborado_fecha = models.DateTimeField(verbose_name='Fecha',default=datetime.now(), null=True, blank=True)

    coordinador_firma = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #coordinador_nombre = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='NOMBRE')
    coordinador_fecha = models.DateTimeField(verbose_name='Fecha',default=datetime.now(),null=True, blank=True)

    vicerrector_firma = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #vicerrector_nombre = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='NOMBRE')
    vicerrector_fecha = models.DateTimeField(verbose_name='Fecha',default=datetime.now(), null=True, blank=True)

    verificado_firma = models.CharField(verbose_name='Firma',max_length=70, null=False)
    #verificado_nombre = models.OneToOneField(Docente, on_delete=models.CASCADE, verbose_name='NOMBRE')
    verificado_fecha = models.DateTimeField(verbose_name='Fecha',default=datetime.now(), null=True, blank=True)

    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.carrera_acta_rega


