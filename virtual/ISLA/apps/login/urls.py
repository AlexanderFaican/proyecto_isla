from django.urls import path
from .import views

urlpatterns = [
	path('',views.ingresar,name='home_page'),
	path('allarticulo',views.ingresar),
	path('all',views.ingresar),
	path('allCategoria',views.ingresar),
	path('logout',views.cerrar, name ='cerrar_sesion')
] 