from django import forms
from apps.Mantenimiento.models import Persona

class FormularioLogin(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput())

class FormularioPersona(forms.ModelForm):
    class Meta:
        model=Persona
        fields= ["nombre_persona","alias_persona",
		"contraseña_persona","correo","telefono_persona","direccion_persona"]