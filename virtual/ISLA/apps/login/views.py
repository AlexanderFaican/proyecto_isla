from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from .forms import FormularioLogin, FormularioPersona
def ingresar(request):
	if request.method =='POST':
		formulario = FormularioLogin(request.POST)
		if formulario.is_valid():
			usuario = request.POST['username']
			clave = request.POST['password']
			user = authenticate(username=usuario,password = clave)
			if user is not None:
				if user.is_active:
					login(request, user)
					return HttpResponseRedirect(reverse('ISLA'))
			print (user)
	else:
		formulario = FormularioLogin()
		formularioP = FormularioPersona()
	return render (request,'index.html',{'formulario':formulario,'formularioP':formularioP})

def cerrar(request):
	logout(request)
	return HttpResponseRedirect(reverse('home_page'))
# Create your views here.