from django.db import models
from datetime import datetime

 
# Create your models here.
class Persona(models.Model):
    id_persona=models.AutoField(primary_key=True)
    nombre_persona = models.CharField(max_length=25, null=False, verbose_name='Apellidos y Nombres')
    alias_persona = models.CharField(max_length=30, unique=True, verbose_name='Usuario')
    contraseña_persona = models.CharField(max_length=30, verbose_name='Contraseña')
    correo = models.EmailField(max_length=50, null=False, verbose_name='E-mail')
    telefono_persona = models.IntegerField(verbose_name='Teléfono', null=False)
    direccion_persona = models.CharField(max_length=100, null=False, verbose_name='Dirección')
    def __unicode__(self):
        return self.nombre_persona
class Mantenimiento(models.Model):
    id_mantenimiento = models.CharField(max_length=30, primary_key=True)
    tipo_dispositivo = models.CharField(max_length=30, null=False, verbose_name='Tipo de Dispositivo')
    descripcion = models.CharField(max_length=500, null=False, verbose_name='Descripción del dispositivo')
    numero_dispositivos = models.IntegerField(verbose_name='Número de Dispositivos')
    accesorios_adicionales = models.CharField(max_length=100, verbose_name='Accesorios adicionales')
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.tipo_dispositivo
class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key=True)
    nombre_cliente = models.OneToOneField(Persona, on_delete=models.CASCADE, verbose_name='Cliente')
    def __unicode__(self):
        return self.nombre_cliente
class Tecnico(models.Model):
    id_tecnico = models.AutoField(primary_key=True)
    nombre_tecnico = models.OneToOneField(Persona, on_delete=models.CASCADE,verbose_name='Técnico')
    def __unicode__(self):
        return self.nombre_tecnico
class Encargado(models.Model):
    id_encargado = models.AutoField(primary_key=True)
    nombre_encargado = models.OneToOneField(Persona, on_delete=models.CASCADE,verbose_name='Encargado')
    def __unicode__(self):
        return self.nombre_encargado
class Ficha_Recepcion(models.Model):
    id_ficha_equipo = models.AutoField(primary_key=True, verbose_name='Número de Ficha')
    cliente = models.OneToOneField(Cliente, on_delete=models.CASCADE)
    tecnico = models.OneToOneField(Tecnico, on_delete=models.CASCADE)
    encargado = models.OneToOneField(Encargado, on_delete=models.CASCADE)
    fecha_recepcion = models.DateTimeField(default=datetime.now(), null=True, blank=True)
    fecha_posible_entrega = models.DateTimeField(default=datetime.now(), null=True, blank=True)
class Ficha_Entrega(models.Model):
    id_ficha_entrega = models.AutoField(primary_key=True, verbose_name='Número de Ficha')
    id_ficha_recepcion=models.OneToOneField(Ficha_Recepcion, on_delete=models.CASCADE, verbose_name='Numero de Ficha de Recepcion')
    cliente = models.OneToOneField(Cliente, on_delete=models.CASCADE)
    tecnico = models.OneToOneField(Tecnico, on_delete=models.CASCADE)
    encargado = models.OneToOneField(Encargado, on_delete=models.CASCADE)
    fecha_recepcion = models.DateTimeField(default=datetime.now(), null=True, blank=True)
    fecha_entrega = models.DateTimeField(default=datetime.now(), null=True, blank=True)
    def __unicode__(self):
        return self.cliente
class Reporte(models.Model):
    id_reporte_entrega = models.OneToOneField(Ficha_Recepcion, on_delete=models.CASCADE, verbose_name='Número de Reporte de Entrega')
    fecha_entrega = models.DateTimeField(auto_now=True)
    costo_servicio = models.DecimalField(max_digits=5, decimal_places=2)
    observaciones = models.CharField(max_length=500)
    recurso = models.ImageField()
    def __unicode__(self):
        return self.fecha_entrega__unicode
