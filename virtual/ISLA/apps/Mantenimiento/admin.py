from django.contrib import admin

# Register your models here.
from .models import Persona, Cliente, Mantenimiento, Tecnico, Encargado, Ficha_Recepcion, Reporte, Ficha_Entrega
class adminficha_recepcion(admin.ModelAdmin):
    list_display = ["id_ficha_equipo","cliente","tecnico","encargado","fecha_recepcion"]
    class Meta:
             model=Ficha_Recepcion
class admin_Persona(admin.ModelAdmin):
    list_display=["nombre_persona","alias_persona","contraseña_persona","correo"]
    class Meta:
             model=Persona
class admin_Ficha_Entrega(admin.ModelAdmin):
    list_display=["id_ficha_recepcion","cliente","tecnico","encargado","fecha_recepcion","fecha_entrega"]
    class Meta:
             model=Ficha_Entrega
class admin_Mantenimmiento(admin.ModelAdmin):
    list_display=["id_mantenimiento","tipo_dispositivo","descripcion","numero_dispositivos","accesorios_adicionales"]
    class Meta:
             model=Mantenimiento
class admin_Reportes(admin.ModelAdmin):
    list_display=["id_reporte_entrega","costo_servicio","observaciones","recurso"]
    class Meta:
             model=Mantenimiento
admin.site.register(Persona, admin_Persona);
admin.site.register(Cliente);
admin.site.register(Mantenimiento, admin_Mantenimmiento);
admin.site.register(Tecnico);
admin.site.register(Encargado);
admin.site.register(Ficha_Recepcion, adminficha_recepcion);
admin.site.register(Ficha_Entrega, admin_Ficha_Entrega);
admin.site.register(Reporte, admin_Reportes);