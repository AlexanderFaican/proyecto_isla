
from django.shortcuts import render, redirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from apps.Mantenimiento.models import Cliente, Encargado, Persona,Ficha_Entrega,Ficha_Recepcion, Tecnico

@login_required 
def list_servicios(request):
    listaFichaRecepcion = Ficha_Recepcion.objects.all()
    listaFichaEntrega = Ficha_Entrega.objects.all()
    clientes = Cliente.objects.all()
    print(clientes)
    tecnicos = Tecnico.objects.all()
    encargados = Encargado.objects.all()
    if request.method =='POST':
        ficha_entrega = Ficha_Recepcion()
        cliente = Persona.objects.get(alias_persona=request.POST['cliente'])
        tecnico = Persona.objects.get(alias_persona=request.POST['tecnico'])
        encargado = Persona.objects.get(alias_persona=request.POST['encargado'])
        
        ficha_entrega.cliente = Cliente.objects.get(nombre_cliente = cliente)
        ficha_entrega.tecnico = Tecnico.objects.get(nombre_tecnico = tecnico)
        ficha_entrega.encargado = Encargado.objects.get(nombre_encargado = encargado)
        ficha_entrega.fecha_recepcion = request.POST['fecha_recepcion']
        ficha_entrega.fecha_posible_entrega = request.POST['fecha_recepcion']
        ficha_entrega.save()
    return render(request,"servicios.html",locals())