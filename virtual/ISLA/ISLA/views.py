from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponseRedirect
from apps.login.forms import FormularioLogin,FormularioPersona

from apps.NotasAsistencia.models import Estudiante, Evento, Nota,Asistencia, Docente, Asignatura,Carrera,Persona
from datetime import datetime
from django.utils import timezone
def home_view(request): 
    if request.method =='POST':
        formulario = FormularioLogin(request.POST)
        formularioP = FormularioPersona(request.POST)
        if formulario.is_valid():
            usuario = request.POST['username']
            clave = request.POST['password']
            user = authenticate(username=usuario,password = clave)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('inicio_sesion'))
            print (user)
        else:
            if formularioP.is_valid():
                persona = Persona()
                persona.nombre_persona = request.POST['nombre_persona']
                persona.alias_persona = request.POST['alias_persona']
                persona.contraseña_persona = request.POST['contraseña_persona']
                persona.correo = request.POST['correo']
                persona.telefono_persona = request.POST['telefono_persona']
                persona.direccion_persona = request.POST['direccion_persona']
                persona.save()
                return redirect(home_view)   
    else:
        formulario = FormularioLogin()
        formularioP = FormularioPersona()
    return render(request,"index.html",{'formulario':formulario,'formularioP':formularioP})

def cerrar(request):
    logout(request)
    return redirect(home_view)



def about_view(request):
    return render(request,"inicio_sesion.html",{})
    
def pricing_view(request):
    listaNotas = Nota.objects.all()
    listaAsistencias = Asistencia.objects.all()
    listaPersonas = Persona.objects.all()
    return render(request,"gestion_a.html",locals())
def inicio_view(request):
    return render(request,"inicio_sesion.html",{})